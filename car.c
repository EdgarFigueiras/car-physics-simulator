// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<stdio.h>

struct wheel
{
	int size;
	//int pressure;
	//int temperature;
} wheel;

struct brake
{
	int size;
	//int temperature;
} brake;

struct body
{
	int weight;
	int length;
	int width;
	int height;
} body;

struct car
{
	//Wheels
	struct wheel wfl;
	struct wheel wfr;
	struct wheel wrl;
	struct wheel wrr;
	
	//Brakes
	struct brake bfl;
	struct brake bfr;
	struct brake brl;
	struct brake brr;

	//Body
	struct body body;

} ferrari458 = { { 245 },{ 245 },{ 305 },{ 305 }, 
				 { 398 },{ 398 },{ 360 },{ 360 }, 
				 {1470, 4571, 1951, 1203} };


main()
{
	printf("1st car: Ferrari 458 \n\n\n");


	printf("Dimensions data: Weight %d, Lenght %d, Width %d, Height %d \n\n", ferrari458.body.weight, ferrari458.body.length, ferrari458.body.width, ferrari458.body.height);
	printf("Wheels data: Front left %d, Front Right %d, Rear Left %d, Rear Right %d \n\n", ferrari458.wfl.size, ferrari458.wfr.size, ferrari458.wrl.size, ferrari458.wrr.size);
	printf("Brakes data: Front left %d, Front Right %d, Rear Left %d, Rear Right %d \n\n", ferrari458.wfl.size, ferrari458.bfr.size, ferrari458.brl.size, ferrari458.brr.size);

	_getch();
	return 0;
}
